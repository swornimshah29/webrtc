package com.example.swornim.webrtc;

import org.webrtc.SessionDescription;

import java.io.Serializable;
import java.util.Locale;

/**
 * Created by swornim on 3/27/18.
 */

public class MSessionDescription implements Serializable {

    public MSessionDescription(){

    }

    private String description;
    private String type;


    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
