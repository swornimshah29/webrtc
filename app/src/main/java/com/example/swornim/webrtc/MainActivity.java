package com.example.swornim.webrtc;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.renderscript.Script;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.webrtc.AudioTrack;
import org.webrtc.Camera1Capturer;
import org.webrtc.Camera1Enumerator;
import org.webrtc.CameraEnumerationAndroid;
import org.webrtc.CameraVideoCapturer;
import org.webrtc.DataChannel;
import org.webrtc.DefaultVideoDecoderFactory;
import org.webrtc.DefaultVideoEncoderFactory;
import org.webrtc.EglBase;
import org.webrtc.IceCandidate;
import org.webrtc.MediaConstraints;
import org.webrtc.MediaStream;
import org.webrtc.PeerConnection;
import org.webrtc.PeerConnectionFactory;
import org.webrtc.RendererCommon;
import org.webrtc.RtpReceiver;
import org.webrtc.SdpObserver;
import org.webrtc.SessionDescription;
import org.webrtc.SurfaceTextureHelper;
import org.webrtc.SurfaceViewRenderer;
import org.webrtc.VideoCapturer;
import org.webrtc.VideoFrame;
import org.webrtc.VideoRenderer;
import org.webrtc.VideoSource;
import org.webrtc.VideoTrack;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class MainActivity extends AppCompatActivity {


    public Handler handler;
    private Button localpeerbtn;
    private Button remotepeerbtn;
    private PeerConnection localPeer;
    private PeerConnection remotePeer;
    private AudioTrack audioTrack;
    private VideoTrack videoTrack;

    private PeerConnectionFactory peerConnectionFactory;
    @Override
    protected void onCreate( Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        localpeerbtn=(Button)findViewById(R.id.localpeerbtn);
        remotepeerbtn=(Button)findViewById(R.id.remotepeerbtn);


        /*starting phase for the peerconnection*/

//        //Create a new PeerConnectionFactory instance.
//        PeerConnectionFactory.Options options = new PeerConnectionFactory.Options();
//        PeerConnectionFactory peerConnectionFactory = new PeerConnectionFactory();

//
//        //Now create a VideoCapturer instance. Callback methods are there if you want to do something! Duh!
//        VideoCapturer videoCapturerAndroid = createVideoCapturer();
//        //Create MediaConstraints - Will be useful for specifying video and audio constraints. More on this later!
//        MediaConstraints constraints = new MediaConstraints();
//
//        //Create a VideoSource instance


        PeerConnectionFactory.InitializationOptions initializationOptions =
                PeerConnectionFactory.InitializationOptions.builder(getApplicationContext())
                        .setEnableVideoHwAcceleration(true)
                        .createInitializationOptions();
        PeerConnectionFactory.initialize(initializationOptions);

        PeerConnectionFactory.Builder builder=PeerConnectionFactory.builder();



        Camera1Enumerator camera1Enumerator=new Camera1Enumerator(false);
        CameraVideoCapturer videoCapturer=null;
        List<CameraEnumerationAndroid.CaptureFormat> supportedFormats;

        for(String eachDeviceName:camera1Enumerator.getDeviceNames()){
            Log.i("mytag",eachDeviceName);


            if(camera1Enumerator.isFrontFacing(eachDeviceName)) {
                Log.i("mytag", "back facing camera found");
                supportedFormats=camera1Enumerator.getSupportedFormats(eachDeviceName);
                for(int i=0;i<supportedFormats.size();i++){
                    Log.i("mytag", "camera properties"+supportedFormats.get(i).framerate);
                    Log.i("mytag", "camera properties"+supportedFormats.get(i).frameSize());
                    Log.i("mytag", "camera properties"+supportedFormats.get(i).imageFormat);
                    Log.i("mytag", "camera properties"+supportedFormats.get(i).height);
                    Log.i("mytag", "camera properties"+supportedFormats.get(i).width);

                }

                videoCapturer = camera1Enumerator.createCapturer(eachDeviceName, null);
            }
        }

        PeerConnectionFactory peerConnectionFactory=builder.createPeerConnectionFactory();
         audioTrack=peerConnectionFactory.createAudioTrack("123",peerConnectionFactory.createAudioSource(new MediaConstraints()));
         videoTrack=peerConnectionFactory.createVideoTrack("12",peerConnectionFactory.createVideoSource(videoCapturer));

        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels;
        int width = displayMetrics.widthPixels;
        Log.i("mytag", "screen height "+height);
        Log.i("mytag", "screen width "+width);

        videoCapturer.startCapture(width,height,60);//frames are starting to  render from the font face came

        SurfaceViewRenderer surfaceViewRendererforlocalpeer=findViewById(R.id.surfaceviewforlocalpeer);
        surfaceViewRendererforlocalpeer.init(EglBase.create().getEglBaseContext(),null);
        surfaceViewRendererforlocalpeer.setMirror(true);

        /*
        To balance the orientation of the camera when the user rotates
        SCALE_ASPECT_BALANCED : auto balance the rotation to adjust the normal face


         */
        surfaceViewRendererforlocalpeer.setScalingType(RendererCommon.ScalingType.SCALE_ASPECT_BALANCED);
        VideoRenderer videoRendererl=new VideoRenderer(surfaceViewRendererforlocalpeer);
        videoTrack.addRenderer(videoRendererl);//view can now see the frames on the surface view

        List<PeerConnection.IceServer> iceServerList=new ArrayList<>();


        MediaConstraints mediaConstraints=new MediaConstraints();
        mediaConstraints.mandatory.add(new MediaConstraints.KeyValuePair("offerToReceiveAudio","true"));//format is fixed
        mediaConstraints.mandatory.add(new MediaConstraints.KeyValuePair("offerToReceiveVideo","true"));

        remotePeer=peerConnectionFactory.createPeerConnection(iceServerList, new PeerConnection.Observer() {
            @Override
            public void onSignalingChange(PeerConnection.SignalingState signalingState) {
                Log.i("mytag", "onsignalingchange remotePeer "+signalingState);

            }

            @Override
            public void onIceConnectionChange(PeerConnection.IceConnectionState iceConnectionState) {
                Log.i("mytag", "on ice connection change remotePeer "+iceConnectionState);

            }

            @Override
            public void onIceConnectionReceivingChange(boolean b) {
                Log.i("mytag", "on ice connection receiving change  remotePeer"+b);

            }

            @Override
            public void onIceGatheringChange(PeerConnection.IceGatheringState iceGatheringState) {
                Log.i("mytag", "on ice gathering change remotePeer "+iceGatheringState);

            }

            @Override
            public void onIceCandidate(IceCandidate iceCandidate) {
                Log.i("mytag", "on ice candicate remotePeer sdp "+iceCandidate.sdp);
                Log.i("mytag", "on ice candicate remotePeer sdpmid "+iceCandidate.sdpMid);
                Log.i("mytag", "on ice candicate remotePeer serverurl "+iceCandidate.serverUrl);
                Log.i("mytag", "on ice candicate remotePeer sdplineindex"+iceCandidate.sdpMLineIndex);

            }

            @Override
            public void onIceCandidatesRemoved(IceCandidate[] iceCandidates) {
                Log.i("mytag", "on ice candidates removed remotePeer"+iceCandidates);

            }

            @Override
            public void onAddStream(MediaStream mediaStream) {
                Log.i("mytag", "remote peer mediastream received "+mediaStream.getId());

            }

            @Override
            public void onRemoveStream(MediaStream mediaStream) {
                Log.i("mytag", "on remove stream remotePeer "+mediaStream);

            }

            @Override
            public void onDataChannel(DataChannel dataChannel) {
                Log.i("mytag", "ondatachannel remotePeer"+dataChannel);

            }

            @Override
            public void onRenegotiationNeeded() {
                Log.i("mytag", "on renegotiation need remote ");

            }

            @Override
            public void onAddTrack(RtpReceiver rtpReceiver, MediaStream[] mediaStreams) {
                Log.i("mytag", "on add track remotePeer"+mediaStreams);

            }
        });
        localPeer = peerConnectionFactory.createPeerConnection(iceServerList, new PeerConnection.Observer() {
            @Override
            public void onSignalingChange(PeerConnection.SignalingState signalingState) {
                Log.i("mytag", "onSignalingChange localPeer");

            }

            @Override
            public void onIceConnectionChange(PeerConnection.IceConnectionState iceConnectionState) {
                Log.i("mytag", "onIceConnectionChange localPeer");

            }

            @Override
            public void onIceConnectionReceivingChange(boolean b) {
                Log.i("mytag", "onIceConnectionReceivingChange localPeer");

            }

            @Override
            public void onIceGatheringChange(PeerConnection.IceGatheringState iceGatheringState) {
                Log.i("mytag", "onIceGatheringChange localPeer");

            }

            @Override
            public void onIceCandidate(IceCandidate iceCandidate) {
                Log.i("mytag", "onIceCandidate localPeer");
                //send this ice candidate to other peer and save to ur self too
                localPeer.addIceCandidate(iceCandidate);


            }

            @Override
            public void onIceCandidatesRemoved(IceCandidate[] iceCandidates) {
                Log.i("mytag", "onIceCandidatesRemoved localPeer");

            }

            @Override
            public void onAddStream(MediaStream mediaStream) {
                Log.i("mytag", "onAddStream localPeer");

            }

            @Override
            public void onRemoveStream(MediaStream mediaStream) {
                Log.i("mytag", "onIceCandidatesRemoved localPeer");

            }

            @Override
            public void onDataChannel(DataChannel dataChannel) {
                Log.i("mytag", "ondatachanel for localpeer");

            }

            @Override
            public void onRenegotiationNeeded() {
                Log.i("mytag", "onRenegotiationNeeded localPeer");

            }

            @Override
            public void onAddTrack(RtpReceiver rtpReceiver, MediaStream[] mediaStreams) {
                Log.i("mytag", "onAddTrack localPeer");

            }
        });



        localpeerbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //creating local stream which is for the sending of audio and video data from phone
                MediaStream localstream = peerConnectionFactory.createLocalMediaStream("102");//a stream pipe for outgoing audio and video
                localstream.addTrack(audioTrack);
                localstream.addTrack(videoTrack);

                localPeer.addStream(localstream);

                //still only the sdp description will be created in local phone#you need to send the signal
                localPeer.createOffer(new SdpObserver() {
                    @Override
                    public void onCreateSuccess(SessionDescription sessionDescription) {
                        Log.i("mytag", "local sessiondescription " + sessionDescription.description + "#" + sessionDescription.type);
                        localPeer.setLocalDescription(new SdpObserver() {
                            @Override
                            public void onCreateSuccess(SessionDescription sessionDescription) {

                            }

                            @Override
                            public void onSetSuccess() {

                            }

                            @Override
                            public void onCreateFailure(String s) {

                            }

                            @Override
                            public void onSetFailure(String s) {

                            }
                        }, sessionDescription);//save the session in the local phone for later use

                        MSessionDescription messageObject=new MSessionDescription();
                        messageObject.setDescription(sessionDescription.description);
                        messageObject.setType(sessionDescription.type.canonicalForm());
                        FirebaseDatabase.getInstance().getReference().child("remote/data").setValue(messageObject);

                        //listen for the callee response of sdp
                        FirebaseDatabase.getInstance().getReference().child("local/data").addChildEventListener(new ChildEventListener() {
                            @Override
                            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                                Log.i("mytag","Response from remote: "+dataSnapshot);
                                if(dataSnapshot.getValue()!=null) {
                                    MSessionDescription mSessionDescription=dataSnapshot.getValue(MSessionDescription.class);
                                    Log.i("mytag", "mapd" + mSessionDescription.getDescription());
                                    Log.i("mytag", "mapt " + mSessionDescription.getType());
                                }
                            }

                            @Override
                            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                            }

                            @Override
                            public void onChildRemoved(DataSnapshot dataSnapshot) {

                            }

                            @Override
                            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {

                            }
                        });

                    }

                    @Override
                    public void onSetSuccess() {

                    }

                    @Override
                    public void onCreateFailure(String s) {

                    }

                    @Override
                    public void onSetFailure(String s) {

                    }
                }, new MediaConstraints());//remote phone is now answering us i.e responding the offer

                //TODO create new mediaconstraints
            }

        });

        remotepeerbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.i("mytag","Request btn ");


                MediaStream remoteStream=peerConnectionFactory.createLocalMediaStream("103");//a stream pipe for outgoing audio and video
                remoteStream.addTrack(audioTrack);
                remoteStream.addTrack(videoTrack);
                remotePeer.addStream(remoteStream);

                //answer the call when request comes
                FirebaseDatabase.getInstance().getReference().child("remote/data").addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        Log.i("mytag",dataSnapshot.getValue()+"");
                        if(dataSnapshot.getValue()!=null && !dataSnapshot.getValue().equals("")) {
                            Log.i("mytag", "datasnap" + dataSnapshot.getValue());

                            MSessionDescription mSessionDescription= dataSnapshot.getValue(MSessionDescription.class);
                            Log.i("mytag", "mapd" + mSessionDescription.getDescription());
                            Log.i("mytag", "mapt " + mSessionDescription.getType());
                            SessionDescription remoteSdp=new SessionDescription(SessionDescription.Type.ANSWER,mSessionDescription.getDescription());


                            remotePeer.createOffer(new SdpObserver() {
                                @Override
                                public void onCreateSuccess(SessionDescription sessionDescription) {
                                    remotePeer.setLocalDescription(new SdpObserver() {
                                        @Override
                                        public void onCreateSuccess(SessionDescription sessionDescription) {

                                        }

                                        @Override
                                        public void onSetSuccess() {

                                        }

                                        @Override
                                        public void onCreateFailure(String s) {

                                        }

                                        @Override
                                        public void onSetFailure(String s) {

                                        }
                                    },sessionDescription);//save remote discription
                                    //listen for firbase new client request
                                    Log.i("mytag","Remote generated: "+sessionDescription.description);

                                    MSessionDescription messageObject=new MSessionDescription();
                                    messageObject.setType(sessionDescription.type.canonicalForm());
                                    messageObject.setDescription(sessionDescription.description);

                                    FirebaseDatabase.getInstance().getReference().child("local/data").setValue(messageObject);

                                }

                                @Override
                                public void onSetSuccess() {

                                }

                                @Override
                                public void onCreateFailure(String s) {

                                }

                                @Override
                                public void onSetFailure(String s) {

                                }
                            },new MediaConstraints());

                            remotePeer.setRemoteDescription(new SdpObserver() {
                                @Override
                                public void onCreateSuccess(SessionDescription sessionDescription) {

                                }

                                @Override
                                public void onSetSuccess() {

                                }

                                @Override
                                public void onCreateFailure(String s) {

                                }

                                @Override
                                public void onSetFailure(String s) {

                                }
                            },remoteSdp);//senderSession..]

                        }


                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

            }
        });



    }//oncreate




}//main class
